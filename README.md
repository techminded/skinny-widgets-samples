# Skinny Widgets Samples

This projects contain demo code for [skinny-widgets](https://bitbucket.org/techminded/skinny-widgets)

## Run

To see examples run http server to serve files

```bash
npx http-server
```

open index page in browser, e.g. [http://127.0.0.1:8080/index.html](http://127.0.0.1:8080/index.html)

and navigate through entries
