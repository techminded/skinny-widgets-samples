

import { JquerySkButton } from '/node_modules/skinny-widgets/src/theme/jquery/jquery-sk-button.js';


export class MyJquerySkButton extends JquerySkButton {

    get prefix() {
        return 'myjquery';
    }

}
