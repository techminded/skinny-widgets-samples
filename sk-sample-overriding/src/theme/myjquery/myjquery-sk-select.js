
import { JquerySkSelect } from '/node_modules/skinny-widgets/src/theme/jquery/jquery-sk-select.js';

export class MyJquerySkSelect extends JquerySkSelect {

    get prefix() {
        return 'myjquery';
    }

}
