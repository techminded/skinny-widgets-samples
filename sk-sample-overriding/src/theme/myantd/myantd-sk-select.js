
import { AntdSkSelect } from '/node_modules/skinny-widgets/src/theme/antd/antd-sk-select.js';

export class MyAntdSkSelect extends AntdSkSelect {

    get prefix() {
        return 'myantd';
    }

}
