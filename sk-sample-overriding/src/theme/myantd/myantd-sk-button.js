
import { AntdSkButton } from '/node_modules/skinny-widgets/src/theme/antd/antd-sk-button.js';

export class MyAntdSkButton extends AntdSkButton {

    get prefix() {
        return 'myantd';
    }

}
