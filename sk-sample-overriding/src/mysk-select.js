
import { SkSelect } from '/node_modules/skinny-widgets/src/sk-select.js';

import { MyAntdSkSelect } from './theme/myantd/myantd-sk-select.js';
import { MyJquerySkSelect } from './theme/myjquery/myjquery-sk-select.js';
import { DefaultSkSelect } from '/node_modules/skinny-widgets/src/theme/default/default-sk-select.js';


export class MySkSelect extends SkSelect {

    get impl() {
        if (! this._impl) {
            if (this.theme === 'myantd') {
                this.impl = new MyAntdSkSelect(this);
            } else if (this.theme === 'myjquery') {
                this.impl = new MyJquerySkSelect(this);
            } else {
                this.impl = new DefaultSkSelect(this);
            }
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

}
