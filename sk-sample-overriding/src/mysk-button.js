
import { SkButton } from '/node_modules/skinny-widgets/src/sk-button.js';


import { DefaultSkButton } from '/node_modules/skinny-widgets/src/theme/default/default-sk-button.js';
import { MyAntdSkButton } from './theme/myantd/myantd-sk-button.js';
import { MyJquerySkButton } from './theme/myjquery/myjquery-sk-button.js';


export class MySkButton extends SkButton {

    get impl() {
        if (! this._impl) {
            if (this.theme === 'myantd') {
                this.impl = new MyAntdSkButton(this);
            } else if (this.theme === 'myjquery') {
                this.impl = new MyJquerySkButton(this);
            } else {
                this.impl = new DefaultSkButton(this);
            }
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }


}
